# usage
```
Usage for ug-oradb:
  FOR SYSADMIN: mainly to create/destroy a database
  ug-oradb create-db <dbname>
  ug-oradb [-f] destroy-db <dbname>
  FOR DEBUGGING: mainly for testing the internal function
  ug-oradb list-snap-unused <dbname>
  ug-oradb get-properties <dbname>
  ug-oradb garbage-snaps <dbname>
  ug-oradb modify-fstab-4-db <dbname>
  ug-oradb modify-fstab-4-snap <dbname>
  FOR DBA:
  ug-oradb list-db-config
  ug-oradb list-snap <dbname>
  ug-oradb create-snap <dbname> [tag]
  ug-oradb destroy-snap <dbname> <snapname>
  ug-oradb rollback <dbname> <snapname>
  
  list-db-config: list the database defined in /etc/ug-oradb.rc
  get-properties: show the properties for a dbnane
  create: create the FS to install the DB on it
  destroy: destroy the FS where the DB is intalled
  list-snap: show snap and where they are mounted
  list-snap-unused: show snap and where they are mounted
  create-snap: snapshot the FS of the DB and mount it somewhere on /backup/<dbname>-<datetime>
  destroy-snap: destroy the snapshot of the DB and unmount it
  rollback: make a rollback and destroy corresponding snapshot
  garbage-snaps: garbage collector 
                 mainly it compares /var/ug-oradb/ with /backup/ to see
                 which snap should be destroyed
  modify-fstab-4-snap: regenerate the fstab to take in account the snaps for a db
  modify-fstab-4-db: regenerate the fstab to take in account the change new/deleted  db
Resource file (/etc/ug-oradb.rc) is missing.
You must create it with someting like
echo 'bd_de_test data:200G:10G ctl_rdo:5G:- admin:5G11G arch:30G:5G export:5G:1G agent:5G:1G' >> /etc/ug-oradb.rc
```
