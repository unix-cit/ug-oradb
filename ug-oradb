#!/bin/bash


progname=$(basename $0)
RC="/etc/${progname}.rc"
exitcode=0

###########################################################
# bash cbr
_B="\e[34m"
_R="\e[31m"
_G="\e[32m"
_N="\e[0m"

function pstd()
{
    echo "$1"
}
function pstdn() {
    echo -en "$1"
}
function pb()
{
    echo -e "${_B}$1${_N}"
}
function pr()
{
    echo -e "${_R}$1${_N}"
}
function pg()
{
    echo -e "${_G}$1${_N}"
}
function pbr()
{
    echo -e "${_B}$1 : ${_R}✘${_N}"
}
function pbg()
{
    echo -e "${_B}$1 : ${_G}✔${_N}"
}
function tab()
{
    if [[ $1 -eq 0 ]]; then
        cat -
    fi
    t="$(printf %${1}s)"
    sed "s|^|${t}|"
}

function date_iso
{
    date +"%Y-%m-%dT%H:%M:%SZ"
}

function date_almost_iso
{
    date +"%Y-%m-%dT%Hh%Mm%SsZ"
}


###########################################################

NOW=$(date_almost_iso)


function dbname_ok_or_exit()
{
  dbname=$1
  do_exit=False
  if [[ $dbname != ${dbname,,} ]]; then
    pr "dbname ($dbname) should be in lower"
    exit
  fi
  if [[ "${dbname}" =~ "snap" ]]; then
    pr "dbname ($dbname) should not contain the word: snap"
    exit
  fi
  if echo "$dbname"| grep -Pvq '^(\d|\w)+$'; then
    echo "dbname should follow grep \""'^(\d|\w)+$'"\""
    exit
  fi
  if ! list_db | grep -wq ${dbname}; then
    pr "Dbname ($dbname) not found in /etc/${progname}.rc"
    pstd "You must create it with someting like:"
    pstd "echo '${dbname} data:200G:10G ctl_rdo:5G:- admin:5G11G arch:30G:5G export:5G:1G agent:5G:1G' >> ${RC}"
    pstd "Exit."
    exit 1
  fi
}


function list_db()
{
  grep -Po "^\S+" /etc/ug-oradb.rc
}


function get_db_properties
{
  dbname=$1
  grep -P "^${dbname}" /etc/ug-oradb.rc  \
  | sed "s|^${dbname} ||" \
  | tr " " "\n" \
  | sed "s|:| |g"
}


function db_destroy
{
  dbname=$1
  vgname="${dbname}"

  pstd "db_destroy"
  pstd "- umount :"
  if mount | grep -Poq "/dev/mapper/${vgname}-\S+"; then
    for mount in $(mount | grep -Po "/dev/mapper/${vgname}-\S+"); do
      pstd $mount | tab 3
    done
    read -p "  umount them [y/N] " do_umount
    if  [[ ${do_umount,,} = y ]]; then
      mount | grep -Po "/dev/mapper/${vgname}-\S+" | xargs -I @ -t umount @| tab 2
    fi
  else
    pstd " - nothing to umount"
  fi

  echo ""
  pstd "- remove lvm : "
  if $(lvdisplay | grep "LV Path" | grep -Poq "/dev/${vgname}/\S+$"); then
    for lvpath in $(lvdisplay | grep "LV Path" | grep -Po "/dev/${vgname}/\S+$");do
      pstd $lvpath | tab 3
    done
    read -p "  remove them  [y/N] " do_remove_lv
    if  [[ ${do_remove_lv,,} = y ]]; then
      for lvpath in $(lvdisplay | grep "LV Path" | grep -Po "/dev/${vgname}/\S+$");do
        pstd $lvpath
        lvremove -f $lvpath
      done
    fi
  else
    pstd " - nothing to remove"
  fi

  echo ""
  pstd "- remove directory hierarchy"
  dir_to_delete="False"
  while read dirname lvsize snapsize; do
    if [[ -d /oracle/${dbname}/${dirname} ]]; then
      pstd /oracle/${dbname}/${dirname} | tab 3
      dir_to_delete="True"
    fi
  done < <( get_db_properties ${dbname} )
  if [[ $dir_to_delete = "True" ]]; then
    pstd "/oracle/${dbname} (if empty)" | tab 3
    read -p "  delete these directories [y/N]: " delete_them
    if [[ ${delete_them,,} == "y" ]]; then
      while read dirname lvsize snapsize; do
        if [[ -d /oracle/${dbname}/${dirname} ]]; then
          pstd "rmdir /oracle/${dbname}/${dirname}" | tab 3
          rmdir /oracle/${dbname}/${dirname} | tab 3
        fi
      done < <( get_db_properties ${dbname} )
    fi
  fi
  if [[ -z "$(ls -A /oracle/${dbname})" ]]; then
    pstd "rmdir /oracle/${dbname}" | tab 3
    rmdir "/oracle/${dbname}" | tab 3
  else
    pstd "Directory (/oracle/${dbname}) not empty. Do not remove it." | tab 2
  fi

  echo ""
  pstd "modify_fstab_4_db ${dbname}"
  modify_fstab_4_db ${dbname}
}


function modify_fstab_4_db()
{
  dbname=$1

  pstd "create a fstab.TMPDB by removing the dbname ($dbname) info and all the snap"
  cat /etc/fstab \
  | grep -Pv -- "# DB \($dbname\)" \
  | grep -Pv "/dev/mapper/${dbname}-\S+" \
  | grep -v -- "-snap-" \
  | grep -Pv "^# SNAP "  > /etc/fstab.TMPDB

  pstd "extract the snap info into /etc/fstab.TMPSNAP"
  cat /etc/fstab \
  | grep -P "^# SNAP |--snap--" > /etc/fstab.TMPSNAP

  pstd "add entry in the fstab.TMPDB for dbname ($dbname)"
  echo "# DB ($dbname)" >> /etc/fstab.TMPDB
  cat /etc/mtab \
  | grep -P "/dev/mapper/${dbname}-\S+" \
  | grep -v -- "-snap-" \
  | sort >> /etc/fstab.TMPDB

  pstd "reinject the snap info (TMPSNAP) in the TMPDB"
  cat /etc/fstab.TMPSNAP >> /etc/fstab.TMPDB
  rm  /etc/fstab.TMPSNAP

  update_fstab_if_needed TMPDB
}


function modify_fstab_4_snap()
{
  dbname=$1

  pstd "create fstab.TMP by removing all info about dbname ($dbname) talking about snap"
  cat /etc/fstab \
  | grep -Pv "^/dev/mapper/${dbname}-\S+--snap--" \
  | grep -Pv -- "# SNAP \(\S+\) of dbname \($dbname\)" > /etc/fstab.TMPSNAP
  pstd "add entry in the fstab.TMP for dbname ($dbname)"
  for snap in $(list_snap_done $dbname); do
    pstd "  snap ($snap)"
    echo "# SNAP ($snap) of dbname ($dbname)" >> /etc/fstab.TMPSNAP
    grep -P "/dev/mapper/${dbname}-\S+--snap--${snap//-/--}\s" /etc/mtab | sort >> /etc/fstab.TMPSNAP
  done

  update_fstab_if_needed TMPSNAP
}

function update_fstab_if_needed()
{
  suffix=$1

  pstd "check if ( /etc/fstab | sort ) = (/etc/fstab.${suffix} | sort )"
  diff <(cat /etc/fstab | sort) <(cat /etc/fstab.${suffix} | sort) &> /dev/null
  if [[ 0 == $? ]]; then
    pstd " - no need to keep a copy of fstab, as they are the same"
  else
    pstd " - copy made on /etc/fstab.${NOW}"
    cp /etc/fstab /etc/fstab.${NOW}
    cp /etc/fstab.${suffix} /etc/fstab
    if [ -d /run/systemd/system ]; then
      systemctl daemon-reload
    fi
  fi
  rm /etc/fstab.${suffix}
}

function db_create
{
    dbname=$1
    shift
    lname_size="$@"
    vgname="${dbname}"

    if [[ ! -d /oracle ]]; then
        mkdir /oracle
    fi
    if [[ ! -d /oracle/${dbname} ]]; then
        mkdir /oracle/${dbname}
    fi

    echo ""
    pstd "Check vgname:"
    if ! vgdisplay | grep -w "VG Name"| grep -w "${vgname}"; then
        pr "No vgname:${vgname} found." | tab 4
        pstd "Exit" | tab 4
        exit 1
    else
        pg "ok" | tab 4
    fi

    echo ""
    pstd "Check lvdisplay"
    if lvdisplay | grep -w "LV Path" | grep -Poq "/dev/${vgname}/\S+$" ; then
        pr "lv already exist for dbname:${dbname}:" | tab 4
        lvdisplay | grep -w "LV Path" | grep -Po "/dev/${vgname}/\S+$"| tab 8
        pstd
        pstd "================== D B - U N S E T U P (start) =========="
        pstd "do: ${progname} db-unsetup ${dbname}"
        # db_destroy "${dbname}"
        pstd "================== D B - U N S E T U P (end) ============"
        exit
    else
        pg "ok" | tab 4
    fi

    echo ""
    pstd "Create LV"
    while read dirname lvsize snapsize; do
      lvname="${dirname}"
      mountpoint="/oracle/${dbname}/${dirname}"
      devpath="/dev/${vgname}/${lvname}"
      pstd "lvcreate --yes -L ${lvsize} -n ${lvname} $vgname" | tab 4
      lvcreate --yes -L ${lvsize} -n ${lvname} $vgname | tab 8
    done < <( get_db_properties ${dbname} )

    echo ""
    pstd "Create the ext4"
    while read dirname lvsize snapsize; do
      lvname="${dirname}"
      mountpoint="/oracle/${dbname}/${dirname}"
      devpath="/dev/${vgname}/${lvname}"
      pstd "mkfs.ext4 '${devpath}'" | tab 4
      mkfs.ext4 "${devpath}" | tab 8
    done < <( get_db_properties ${dbname} )

    echo ""
    pstd "Create the mountpoint dir"
    while read dirname lvsize snapsize; do
      lvname="${dirname}"
      mountpoint="/oracle/${dbname}/${dirname}"
      devpath="/dev/${vgname}/${lvname}"
      if [[ ! -d ${mountpoint} ]]; then
        pstd "mkdir '${mountpoint}'" | tab 4
        mkdir "${mountpoint}" | tab 4
      fi
    done < <( get_db_properties ${dbname} )

    echo ""
    pstd "Mount them"
    while read dirname lvsize snapsize; do
      lvname="${dirname}"
      mountpoint="/oracle/${dbname}/${dirname}"
      devpath="/dev/${vgname}/${lvname}"
      pstd "mount -t ext4 '${devpath}' '${mountpoint}'"| tab 4
      mount -t ext4 "${devpath}" "${mountpoint}" | tab 8
    done < <( get_db_properties ${dbname} )

    echo ""
    pstd "modify_fstab_4_db ${dbname}"
    modify_fstab_4_db ${dbname}
}


function write_snap_done
{
  dbname=$1
  snapname=$2
  if [[ ! -d /var/${progname} ]]; then
    mkdir -p /var/${progname}
  fi
  pstd "touch /var/${progname}/${dbname}-${snapname}"
  touch "/var/${progname}/${dbname}-${snapname}"
  return $?
}


function erase_snap_done
{
  local dbname=$1
  local snapname=$2
  local retour=0
  if [[ -f /var/${progname}/${dbname}-${snapname} ]]; then
    pstd "rm /var/${progname}/${dbname}-${snapname}" | tab 2
    rm /var/${progname}/${dbname}-${snapname}
  else
    pr "No snapshot '${snapname}' found for db ${dbname}" | tab 2
    retour=1
  fi
  return $retour
}


function list_snap_done()
{
  dbname=$1
  if [[ ! -d /var/${progname} ]]; then
    mkdir -p /var/${progname}
  fi
  ls -1 /var/${progname} | grep -P "^${dbname}-" | sed "s|^${dbname}-||"
}


function destroy_snap()
{
  local dbname=$1
  local snapname=$2
  pstd "erase_snap_done $dbname $snapname"
  erase_snap_done $dbname $snapname
  if [ $? != 0 ]; then
    exitcode=1
  fi

  echo ""
  pstd "garbage_snap $dbname $snapname"
  garbage_snap $dbname $snapname | tab 2
}

function rollback_check_prerequisite()
{
  local dbname=$1
  local snapname=$2
  local requisite=0
  pstd "Check prerequisite"
  while read dirname lvsize snapsize; do
    if [[ $snapsize = "-" ]];then
      continue
    fi
    lvname="${dirname}"
    vglvname="${dbname}/${dirname}"
    lvsnapname="${lvname}-snap-${snapname}"
    mountpoint_lv="/oracle/${dbname}/${dirname}"
    mountpoint_snap="/backup/${dbname}/${snapname}/${dirname}"
    devpath="/dev/${dbname}/${lvname}"
    pstd "$mountpoint_lv" | tab 2
    lvdisplay /dev/$dbname/$lvsnapname > /dev/null 2>&1
    if [ $? != 0 ]; then
      pstdn "LVM Snapshot $lvsnapname (VG $dbname) not found - " | tab 4
      pr "FAILED"
      requisite=1
    else
      lvmsize=$(lvs -o lv_name,data_percent --separator=' ' --noheadings  -S "lv_attr=~[^s.*]" $dbname | grep $lvsnapname | awk '{print $2}')
      if [ ${lvmsize%.*} -ge 100 ]; then
        pstdn "LVM Snapshot $lvsnapname (VG $dbname) full at 100% - " | tab 4
        pr "INCONSISTENT"
        requisite=1
      else
        pstdn "LVM Snapshot $lvsnapname (VG $dbname) - " | tab 4
        pg "OK"
      fi
      
      /usr/sbin/lsof $mountpoint_snap > /dev/null 2>&1
      if [ $? = 1 ]; then
        pstdn "No process running on $mountpoint_snap - " | tab 4
        pg "OK"
      else
        pstdn "Processes running on $mountpoint_snap - " | tab 4
        pr "FAILED"
        requisite=1
      fi
    fi
    
    /usr/sbin/lsof $mountpoint_lv > /dev/null 2>&1
    if [ $? = 1 ]; then
      pstdn "No process running on $mountpoint_lv - " | tab 4
      pg "OK"
    else
      pstdn "Processes running on $mountpoint_lv - " | tab 4
      pr "FAILED"
      requisite=1
    fi
  done < <( get_db_properties ${dbname} )
  
  return $requisite
}

function check_newer_snapshot()
{
  local dbname=$1
  local snapname=$2
  local retour=0
  pstd "Checking for newer snapshot"
  if ls -1 /var/${progname}| grep -v ${dbname}-${snapname} > /dev/null 2>&1 ; then
    for mysnapshot in $(ls -1 /var/${progname}| grep -v $snapname | grep -P "^${dbname}-" | sed "s|^${dbname}-||" 2> /dev/null); do
      pstd "Snapshot $mysnapshot found" | tab 2
    done
  else
    pstd "No other snapshot found" | tab 2
  fi
  return $retour
}

function rollback()
{
  local dbname=$1
  local snapname=$2
  pstd "rollback $dbname $snapname"
  
  if [[ -f /var/${progname}/${dbname}-${snapname} ]]; then
    rollback_check_prerequisite $dbname $snapname
    if [ $? = 0 ]; then
      pg "Prerequisite ok, let's start!"
      check_newer_snapshot $dbname $snapname
      if [ $? = 0 ]; then
        pstd "Rollback"
        while read dirname lvsize snapsize; do
          if [[ $snapsize = "-" ]];then
            continue
          fi
          lvname="${dirname}"
          vglvname="${dbname}/${dirname}"
          lvsnapname="${lvname}-snap-${snapname}"
          mountpoint_lv="/oracle/${dbname}/${dirname}"
          mountpoint_snap="/backup/${dbname}/${snapname}/${dirname}"
          devpath="/dev/${dbname}/${lvname}"
          pstd "${lvname} rollback" | tab 2
          umount $mountpoint_snap
          if [ $? = 0 ]; then
            pstdn "umount $mountpoint_snap - " | tab 4
            pg "OK"
            umount $mountpoint_lv
            if [ $? = 0 ]; then
              pstdn "umount $mountpoint_lv - " | tab 4
              pg "OK"
              lvconvert --merge /dev/${dbname}/$lvsnapname
              if [ $? = 0 ]; then
                pstdn "Rollback using lvconvert - " | tab 4
                pg "OK"
                lvchange -a n /dev/${dbname}/$lvname
                lvchange -a y /dev/${dbname}/$lvname
                if [ $? = 0 ]; then
                  mount $mountpoint_lv 2> /dev/null
                else
                  pstdn "Error during lvchange -a y /dev/${dbname}/$lvname - " | tab 4
                  pr "FAILED"
                  exitcode=1
                fi
              else
                pstdn "Error during lvconvert --merge /dev/${dbname}/$lvsnapname - " | tab 4
                pr "FAILED"
                exitcode=1
              fi
            else
              pstdn "Error during umount $mountpoint_lv - " | tab 4
              pr "FAILED"
              exitcode=1
            fi
          else
            pstdn "Error during umount $mountpoint_snap - " | tab 4
            pr "FAILED"
            exitcode=1
          fi
        done < <( get_db_properties ${dbname} )
        if [ $exitcode = 0 ]; then
          # All is OK, we make a cleanup
          pstd "Clean up"
          if [[ -d /backup/${dbname}/${snapname} ]]; then
            pstd "rm -Rf /backup/${dbname}/${snapname}" | tab 2
            rm -Rf /backup/${dbname}/${snapname} | tab 2
          fi
          echo ""
          pstd "modify_fstab_4_snap ${dbname}" | tab 2
          modify_fstab_4_snap ${dbname} | tab 2
          rm -f /var/${progname}/${dbname}-${snapname}
        fi
      fi
    else
      pr "Prerequisite FAILED - EXIT"
      exitcode=1
    fi
  else
    pr "No snapshot '${snapname}' found for db ${dbname} (no file /var/${progname}/${dbname}-${snapname})" | tab 2
    exitcode=1
  fi
}

function list_snap_unused()
{
  dbname=$1
  comm -3 <(ls -1 /backup/${dbname}/) <(list_snap_done $dbname)
}

function garbage_snap()
{
  dbname=$1
  snapname=$2

  vgname=$dbname
  pstd "Umount snap"
  for mountpoint in $(mount | awk '{print $3}' | grep -P "^/backup/${dbname}/${snapname}"); do
      pstd ${mountpoint} | tab 2
      umount "${mountpoint}" | tab 2
  done

  echo ""
  pstd "Remove old snapshot"
  for lv in $(lvdisplay -C | grep -P "^\s*\S+-snap-${snapname}\s+${dbname}\s" | grep -Po "\S+-snap-${snapname}"); do
      pstd "lvremove --yes \"${vgname}/${lv}\"" | tab 2
      lvremove --yes "${vgname}/${lv}" | tab 2
  done

  echo ""
  pstd "Remove old hierarchy unused"
  while read dirname lvsize snapsize; do
    if [[ $snapsize = "-" ]];then
      continue
    fi
    lvname="${dirname}"
    vglvname="${vgname}/${dirname}"
    lvsnapname="${lvname}-snap-${snapname}"
    mountpoint_lv="/oracle/${dbname}/${dirname}"
    mountpoint_snap="/backup/${dbname}/${snapname}/${dirname}"
    devpath="/dev/${vgname}/${lvname}"
    if [[ -d ${mountpoint_snap} ]]; then
      pstd "rmdir ${mountpoint_snap}" | tab 2
      rmdir ${mountpoint_snap} | tab 2
    fi
  done < <( get_db_properties ${dbname} )
  if [[ -d /backup/${dbname}/${snapname} ]]; then
    pstd "rmdir /backup/${dbname}/${snapname}" | tab 2
    rmdir /backup/${dbname}/${snapname} | tab 2
  fi

  echo ""
  pstd "modify_fstab_4_snap ${dbname}"
  modify_fstab_4_snap ${dbname}
}

function garbage_snaps()
{
  dbname=$1
  for snapname in $(list_snap_unused $dbname); do
    pstd "garbage_snap $dbname $snapname"
    garbage_snap $dbname $snapname
  done
}

function create_snap()
{
  dbname=$1
  vgname=$dbname
  tag=$2

  if [[ -z ${tag} ]]; then
    snapname=${NOW}
  else
    snapname=${NOW}-${tag}
  fi

  echo ""
  pstd "SNAPSHOT NAME  = $snapname"
  echo ""
  pstd "Create the hierarchy that holds the backup"
  while read dirname lvsize snapsize; do
    if [[ $snapsize = "-" ]];then
      continue
    fi
    lvname="${dirname}"
    vglvname="${vgname}/${dirname}"
    lvsnapname="${lvname}-snap-${snapname}"
    mountpoint_lv="/oracle/${dbname}/${dirname}"
    mountpoint_snap="/backup/${dbname}/${snapname}/${dirname}"
    devpath="/dev/${vgname}/${lvname}"
    if [[ ! -d ${mountpoint_snap} ]]; then
      pstd "mkdir -p ${mountpoint_snap}" | tab 2
      mkdir -p ${mountpoint_snap} 2> /dev/null
      if [ $? != 0 ]; then
        exitcode=1
        pr "[ Failed ]" | tab 4
      else
        pg "[ OK ]" | tab 4
      fi
    fi
  done < <( get_db_properties ${dbname} )
  if [ $exitcode != 0 ]; then
    pr "Issue during directory creation, we stop the process"
    echo ""
    pr "ROLLBACK"
    garbage_snap $dbname $snapname
    pr "END ROLLBACK"
    exit $exitcode
  fi
  
  echo ""
  pstd "Snap them"
  while read dirname lvsize snapsize; do
    if [[ $snapsize = "-" ]];then
      continue
    fi
    lvname="${dirname}"
    vglvname="${vgname}/${dirname}"
    lvsnapname="${lvname}-snap-${snapname}"
    mountpoint_lv="/oracle/${dbname}/${dirname}"
    mountpoint_snap="/backup/${dbname}/${snapname}/${dirname}"
    devpath="/dev/${vgname}/${lvname}"
    pstd "${dirname} with : lvcreate -pr --snapshot -L \"${snapsize}\" --name \"${lvsnapname}\" \"${vglvname}\"" | tab 2
    lvcreate -pr --snapshot -L "${snapsize}" --name "${lvsnapname}" "${vglvname}"
    if [ $? != 0 ]; then
      exitcode=1
      pr "[ Failed ]" | tab 4
    else
      pg "[ OK ]" | tab 4
    fi
  done < <( get_db_properties ${dbname} )
  if [ $exitcode != 0 ]; then
    pr "Issue during snapshot creation, we stop the process."
    echo ""
    pr "ROLLBACK"
    garbage_snap $dbname $snapname
    pr "END ROLLBACK"
    exit $exitcode
  fi

  echo ""
  pstd "Mount them"
  while read dirname lvsize snapsize; do
    if [[ $snapsize = "-" ]];then
      continue
    fi
    lvname="${dirname}"
    vglvname="${vgname}/${dirname}"
    lvsnapname="${lvname}-snap-${snapname}"
    mountpoint_lv="/oracle/${dbname}/${dirname}"
    mountpoint_snap="/backup/${dbname}/${snapname}/${dirname}"
    devpath="/dev/${vgname}/${lvname}"
    pstd "${dirname} with: mount -o ro \"/dev/${vgname}/${lvsnapname}\" \"${mountpoint_snap}\"" | tab 2
    mount -o ro "/dev/${vgname}/${lvsnapname}" "${mountpoint_snap}"
    if [ $? != 0 ]; then
      exitcode=1
      pr "[ Failed ]" | tab 4
    else
      pg "[ OK ]" | tab 4
    fi
  done < <( get_db_properties ${dbname} )
  if [ $exitcode != 0 ]; then
    pr "Issue during mount SNAP in RO, we stop the process."
    echo ""
    pr "ROLLBACK"
    garbage_snap $dbname $snapname
    pr "END ROLLBACK"
    exit $exitcode
  fi

  echo ""
  pstd "write_snap_done $dbname ${snapname}"
  write_snap_done $dbname ${snapname}

  echo ""
  pstd "modify_fstab_4_snap ${dbname}"
  modify_fstab_4_snap ${dbname}
}

function usage()
{
  pstd "Usage for ${progname}:"
  pstd "  FOR DEBUGGING: mainly for testing the internal function"
  pstd "    ${progname} list-snap-unused <dbname>"
  pstd "    ${progname} get-properties <dbname>"
  pstd "    ${progname} garbage-snaps <dbname>"
  pstd "    ${progname} modify-fstab-4-db <dbname>"
  pstd "    ${progname} modify-fstab-4-snap <dbname>"
  pstd "  FOR SYSADMIN: mainly to create/destroy a database"
  pstd "    ${progname} create-db <dbname>"
  pstd "    ${progname} [-f] destroy-db <dbname>"
  pstd "  FOR DBA:"
  pstd "    ${progname} list-db-config"
  pstd "    ${progname} list-snap <dbname>"
  pstd "    ${progname} create-snap <dbname> [tag]"
  pstd "    ${progname} destroy-snap <dbname> <snapname>"
  pstd "    ${progname} rollback <dbname> <snapname>"
  pstd ""
  pstd "  list-db-config: list the database defined in ${RC}"
  pstd "  get-properties: show the properties for a dbnane"
  pstd "  create: create the FS to install the DB on it"
  pstd "  destroy: destroy the FS where the DB is intalled"
  pstd "  list-snap: show snap and where they are mounted"
  pstd "  list-snap-unused: show snap and where they are mounted"
  pstd "  create-snap: snapshot the FS of the DB and mount it somewhere on /backup/<dbname>-<datetime>"
  pstd "  destroy-snap: destroy the snapshot of the DB and unmount it"
  pstd "  rollback: make a rollback and destroy corresponding snapshot"
  pstd "  garbage-snaps: garbage collector "
  pstd "                 mainly it compares /var/${progname}/ with /backup/ to see"
  pstd "                 which snap should be destroyed"
  pstd "  modify-fstab-4-snap: regenerate the fstab to take in account the snaps for a db"
  pstd "  modify-fstab-4-db: regenerate the fstab to take in account the change new/deleted  db"

  if [[ ! -s $RC ]]; then
    pr "Resource file ($RC) is missing."
    pstd "You must create it with someting like:"
    pstd "echo 'bd_de_test data:200G:10G ctl_rdo:5G:- admin:5G11G arch:30G:5G export:5G:1G agent:5G:1G' >> ${RC}"
  fi
}

# check if we execute in Root
if [ $(id -u) != 0 ]; then
  pr "${progname} must be executed by root user"
  exit 1
fi

case "$1" in
    create-db)
      shift
      dbname=$1
      dbname_ok_or_exit "$dbname"
      shift
      lname_size="$@"
      db_create ${dbname} $lname_size
      ;;
    destroy-db)
      shift
      if [[ $1 != "-f" ]]; then
        dbname=$1
        dbname_ok_or_exit "$dbname"
        read -p "Are you sur you want to destroy the db (${dbname}) [y/N] : " do_destroy
        if [[ ${do_destroy,,} != "y" ]]; then
          pstd "Abort."
          exit 0
        fi
      else
        dbname=$1
        dbname_ok_or_exit "$dbname"
        pstd "Option '-f' given to destroy ${dbname}. Force."
      fi
      db_destroy ${dbname}
      ;;
    list-snap)
      shift
      dbname=$1
      dbname_ok_or_exit "$dbname"
      list_snap_done ${dbname}
      ;;
    list-snap-unused)
      shift
      dbname=$1
      dbname_ok_or_exit "$dbname"
      list_snap_unused ${dbname}
      ;;
    create-snap)
      shift
      dbname=$1
      dbname_ok_or_exit "$dbname"
      tag=${2^^}
      create_snap ${dbname} ${tag}
      ;;
    destroy-snap)
      shift
      dbname=$1
      dbname_ok_or_exit "$dbname"
      snapname=$2
      if [[ -z $dbname ]]; then usage; exit 1; fi
      if [[ -z $snapname ]]; then usage; exit 1; fi
      destroy_snap ${dbname} ${snapname}
      ;;
    rollback)
      shift
      dbname=$1
      dbname_ok_or_exit "$dbname"
      snapname=$2
      if [[ -z $dbname ]]; then usage; exit 1; fi
      if [[ -z $snapname ]]; then usage; exit 1; fi
      rollback ${dbname} ${snapname}
      ;;
    garbage-snaps)
      shift
      dbname=$1
      dbname_ok_or_exit "$dbname"
      garbage_snaps ${dbname}
      ;;
    list-db-config)
      list_db
      ;;
    get-properties)
      shift
      dbname=$1
      dbname_ok_or_exit "$dbname"
      get_db_properties ${dbname}
      ;;
    modify-fstab-4-db)
      shift
      dbname=$1
      dbname_ok_or_exit "$dbname"
      modify_fstab_4_db ${dbname}
      ;;
    modify-fstab-4-snap)
      shift
      dbname=$1
      dbname_ok_or_exit "$dbname"
      modify_fstab_4_snap ${dbname}
      ;;
    *)
      usage
      ;;
esac

exit $exitcode
